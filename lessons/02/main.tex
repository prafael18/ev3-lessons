%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------
\documentclass{beamer}

\mode<presentation> {
    \usetheme{Madrid}
    \usecolortheme{dolphin}
    
    %\setbeamertemplate{footline} % To remove the footer line in all slides uncomment this line
    %\setbeamertemplate{footline}[page number] % To replace the footer line in all slides with a simple slide count uncomment this line
    %\setbeamertemplate{navigation symbols}{} % To remove the navigation symbols from the bottom of all slides uncomment this line
}
\usepackage[normalem]{ulem}
\usepackage[english]{babel}
\usepackage{fontawesome}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{subcaption}
\usepackage{tabularx}
    \newcolumntype{L}{>{\hsize=.5\hsize}X}
    
\AtBeginSection[] {
    \begin{frame}<beamer>
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
    \end{frame}
}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[Lesson 2]{Programming with Mindstorms EV3} % The short title appears at the bottom of every slide, the full title is only on the title page
\subtitle{Lesson 2}
\author{Rafael Figueiredo Prudencio} % Your name
\institute[UNICAMP] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
    Universidade Estadual de Campinas \\ % Your institution for the title page
    \medskip
    \textit{rafael.prudencio@gmail.com} % Your email address
}
\date{\today} % Date, can be changed to a custom date

\begin{document}
\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\section{Introduction}
\subsection{Recap}

\begin{frame}
\frametitle{Recap}
What do you remember from last class?
\begin{columns}[c]
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item<2-> \textbf{Port View} What is it? How do we use it?
            \item<3-> \textbf{Move Blocks} What kind of move blocks are there? What's the difference? What modes do they operate on?
            \item<4-> \textbf{Touch Sensor} What's the difference between a sensor block and a control block? What are the different states and modes?
            \item<5-> \textbf{Turning} What kinds of turns can you program?
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{center}
            \includegraphics<2>[width=0.7\textwidth]{images/port_view/port_view.png}
            \includegraphics<3>[width=0.7\textwidth]{images/move/move_steering_block_labeled.png}
            \includegraphics<4>[width=0.7\textwidth]{images/touch/touch_sensor_data.png}
            \includegraphics<5>[width=0.7\textwidth]{images/robot.png}
        \end{center}
    \end{column}
\end{columns}
\end{frame}

\subsection{Lesson Overview}
\begin{frame}{Lesson Overview}
    \begin{enumerate}
        % \item Learn how to read sensor data and use it for debugging with \textbf{Port View} 
        % \item Understand the different \textbf{Action Blocks} that can be used with the motors and use them in different modes
        % \item Learn how to use the \textbf{Touch Sensor} to change the control flow of your program and understand the meaning of its states.
        \item Learn how to use all of the \textbf{Flow Blocks}, including \textbf{Loops} and \textbf{Switches}
        \item Understand the different modes of the \textbf{Color Sensor}
        \item Experiment with brick-related \textbf{Action Blocks} to add images to the display and play sounds
        \item Program a robot to \textbf{follow a line}
    \end{enumerate}
\end{frame}

\section{Flow Blocks}
\subsection{Start Block}
\begin{frame}
\frametitle{Start Block}
\textbf{Flow Blocks} allow you to modify the execution order of your program.
The \textbf{Start Block} marks the beginning of a programming block sequence in your program. Your program can have more than one sequence. All sequences with a \textbf{Start Block} will start automatically when a program is run, and the sequences will run at the same time.
\begin{figure}
    \centering
    \includegraphics[height=0.53\textheight]{images/flow/start_block_example.png}
    \caption{Two Start Blocks that can be run simultaneously}
\end{figure}
\end{frame}

\subsection{Wait Block}
\begin{frame}
\frametitle{Wait Block}
The \textbf{Wait Block} makes your program wait for an event before continuing to the next block in the sequence. This event can be a timer expiring, a sensor reaching a certain value or a change in a sensor's value.
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{itemize}
        \item \textbf{Time} - Wait for the amount of time specified in seconds
        \item \textbf{Sensor Compare} - Each sensor type has one or more compare modes that can be used to wait for it to reach a specified threshold
        \item \textbf{Sensor Change} - Some sensors allow you to wait for a sensor to change to any value that is different than the value at the start of the block 
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/flow/wait_block.png}
        \end{figure}    
    
    \end{column}
\end{columns}
\end{frame}

\subsection{Loop Block}
\begin{frame}{Loop Block}
The \textbf{Loop Block} is a container that can hold a sequence of programming blocks. It will make the sequence of blocks inside repeat. You can program a loop to repeat forever, a certain number of times or until an event happens and a condition is met.
The Loop Block supports modes
\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item \textbf{Unlimited} - Sequence is repeated forever
            \item \textbf{Count} - Input specifies how many times to repeat sequence
            \item \textbf{Time} - Input specifies how long to repeat sequence in seconds. The time limit is tested only at the \emph{end} of the sequence
            \item \textbf{Logic} - Repeats until it reads \emph{True} as its input
            \item \textbf{Sensor} - Continues until a sensor condition is met
        \end{itemize}    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[width=0.7\textwidth]{images/flow/loop_block.png}
            \caption{Loop Block set to mode \textbf{Count}}
        \end{figure}
    \end{column}
\end{columns}

\end{frame}

\subsection{Switch Block}
\begin{frame}{Switch Block}
A \textbf{Switch Block} is a container that can hold two or more sequences of programming blocks. Each sequence is called a \textbf{Case}. A test at the beginning of the \textbf{Switch} determines which \textbf{Case} will run. Only one \textbf{Case} will run each time the \textbf{Switch is executed}
\begin{figure}
    \centering
    \includegraphics[height=0.6\textheight]{images/flow/switch_block.png}
    \caption{Switch color flashing on EV3 brick}
\end{figure}
\end{frame}

\subsection{Loop Interrupt Block}
\begin{frame}
\frametitle{Loop Interrupt Block}
The \textbf{Loop Interrupt Block} makes a \textbf{Loop Block} end. No more blocks in the loop sequence will execute, and the program will continue with any blocks that are after the loop. You can specify which \textbf{Loop Block} to interrupt by using its \textbf{Loop Name}.
\begin{figure}
    \centering
    \includegraphics[height=0.53\textheight]{images/flow/loop_interrupt_block.png}
    \caption{Loop interrupt block used to halt execution before the loop count is reached}
\end{figure}
\end{frame}

\subsection{Flow Challenge}
\begin{frame}
\frametitle{Guard Dog Challenge}
\begin{block}{Challenge 1}
    We want to make sure our robot dog scares off any intruders from the house. Program your robot to move back and forth twice, while growling at any intruders. Even after its done moving, the robot should keep growling to make sure the intruders keep their distance. \faClockO~ 10 min. \\
    \faFileArchiveO~flow\\
    \faFileCodeO~guard\_dog\_challenge\\
\end{block}

\begin{columns}[t]
    \begin{column}{0.4\textwidth}
        \begin{alertblock}<2->{Hints}
            How can we achieve simultaneous execution? What does the \textbf{Start Block} do?
        \end{alertblock}
    \end{column}
    \begin{column}{0.6\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.3\textheight]{images/flow/guard_dog_challenge_diagram.pdf}
            \caption{Move back and forth while growling}
        \end{figure}
    \end{column}

\end{columns}
\end{frame}

\begin{frame}
\frametitle{Guard Dog Challenge}
    \begin{exampleblock}{Solution}
    In order to achieve simultaneous execution, we had to use two \textbf{Start Blocks} or have two sequences branching from the same one. In one of the sequences, a \textbf{growling} sound should play on an infinite loop. On the other sequence, the robot should move back and forth twice, preferably with a \textbf{Loop Block}.
    \end{exampleblock}
    \begin{figure}
        \centering
        \includegraphics[height=0.44\textheight]{images/flow/guard_dog_challenge_solution.png}
        \caption{Two \textbf{Start Blocks} are used to execute both sequences simultaneously}
    \end{figure}
\end{frame}

\begin{frame}
\frametitle{Tickling Challenge}
\begin{block}{Challenge 2}
    Program the robot to smile when tickled. When pressing the \textbf{Touch Sensor} the brick's display should showcase a \textbf{smiling} image. Otherwise, the brick should show a \textbf{sad} image. After $10$ seconds of this behavior, the program should terminate with a \textbf{big smile} on the display.
    \faClockO~15 min. \\
    \faFileCodeO~tickling\_challenge\\
\end{block}

\begin{figure}
    \centering
    \includegraphics[height=0.45\textheight]{images/flow/tickling_challenge_diagram.pdf}
    \caption{Smile when tickled for $10$ seconds then keep a big smile}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Tickling Challenge}
    \begin{exampleblock}{Solution}
    To switch behaviors in response to the touch sensor, we should use a \textbf{Switch Block} wrapped inside a \textbf{Loop Block} that lasts for $10$ seconds. At the end of the \textbf{Loop}, we should add a final image with a big smile to display.
    \end{exampleblock}
    \begin{figure}
        \centering
        \includegraphics[height=0.5\textheight]{images/flow/tickling_challenge_solution.png}
        \caption{A \textbf{Switch Block} wrapped inside a timed \textbf{Loop}}
    \end{figure}
\end{frame}


\section{Color Sensor}
\subsection{Modes}
\begin{frame}{Color Sensor}
    The \textbf{Color Sensor} can detect the color or intensity of light that enters the small window on the face of the sensor. It can be used in three different modes: \textbf{Color Mode}, \textbf{Reflected Light Mode}, and \textbf{Ambient Light Intensity Mode}. \\~\\
    
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{images/color/color_block.png}
        \caption{Color block modes}
    \end{figure}
\end{frame}

% \begin{columns}
%     \begin{column}{0.5\textwidth}
%     \end{column}
%     \begin{column}{0.5\textwidth}
%     \end{column}
% \end{columns}

\begin{frame}{Color Mode}
    \textbf{Color Mode} detects the color of a nearby surface. The sensor can detect seven colors: black, blue, green, yellow, red, white and brown. If none of these colors are detected, the sensor outputs "No Color".
    
    \begin{columns}[t]
        \begin{column}{0.4\textwidth}
            \begin{alertblock}{Tip}
                The object or surface should be very close to the sensor (but not touching it) to be detected accurately
            \end{alertblock}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{figure}
                \centering
                \includegraphics[height=0.55\textheight]{images/color/wait_on_color_mode.png}
                \caption{Color options when in color mode}
            \end{figure}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}{Reflected Light Intensity Mode}
    \textbf{Reflected Light Intensity Mode} detects the light that enters the sensor \emph{while producing its own light source} and outputs a percentage from $0$ to $100$, with $0$ being very dark, and $100$ being very bright. 

    \begin{columns}[t]
        \begin{column}{0.4\textwidth}
            \begin{block}{Note}
                This mode measures the total amount of light entering the sensor, including the reflection of the red LED and any lights in the room. Positioning the sensor close to the surface reduces the effects of outside light sources.
            \end{block}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{figure}
                \centering
                \includegraphics[height=0.45\textheight]{images/color/reflected_light.png}
                \caption{Color sensor operating on Reflected Light Intensity Mode}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Ambient Light Intensity Mode}
    \textbf{Ambient Light Intensity Mode} detects the light that enters the sensor \emph{without producing its own light source} and outputs a percentage from $0$ to $100$, with $0$ being very dark, and $100$ being very bright.\\
    When in this mode, the sensor lights up a dim blue light. This light shouldn't affect the measurement unless an object is very close the sensor.
    
    \begin{figure}
        \centering
        \includegraphics[height=0.45\textheight]{images/color/ambient_light.png}
        \caption{Color sensor operating on Ambient Light Intensity Mode}
    \end{figure}

\end{frame}

\subsection{Challenge}
\begin{frame}{Color Racing Challenge}
    \begin{block}{Challenge 3}
        We want to build a robot capable of functioning on different racetracks. The finish lines can be \textbf{green}, \textbf{blue} or \textbf{red}. After crossing the finish line, the racer should scream what color he saw when he crossed it. \\
        \faFileArchiveO~color \\
        \faFileCodeO~racing\_challenge
    \end{block}
    
    \begin{figure}
        \centering
        \includegraphics[height=0.45\textheight]{images/color/color_racing_challenge_diagram.pdf}
        \caption{Color racing challenge diagram}
    \end{figure}
\end{frame}

\begin{frame}{Color Racing Challenge}
    \begin{exampleblock}{Solution}
        In order to move the robot until we detect a colored line, we have to use the \textbf{On} and \textbf{Off} modes of the \textbf{Motor Block}. The \textbf{Wait On Block} should be set to mode \textbf{Compare} with the colors \textbf{green}, \textbf{blue}, and \textbf{red} selected. Finally, a \textbf{Switch Block} should be used to play the appropriate sound according the the color read by the sensor.
    \end{exampleblock}
    
    \begin{figure}
        \centering
        \includegraphics[height=0.45\textheight]{images/color/color_racing_challenge_solution.png}
        \caption{Solution to color racing challenge}
    \end{figure}
\end{frame}

\begin{frame}{Line Following}
How would you program a robot to follow a straight line until its end?
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{itemize}
        \item What kind of questions can we ask?
        \item<2-> Are we on the line? If so, continue. Otherwise, stop.
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.7\textheight]{images/color/follow_straight_line.pdf}
            \caption{Follow straight line}
            \label{fig:my_label}
        \end{figure}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}{Line Following}
How can we change our algorithm to follow a curved line?
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item If on black, keep going straight.
        \item Otherwise, turn left to stay above the line
    \end{enumerate}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.7\textheight]{images/color/follow_left_curve.pdf}
            \caption{Follow left curve}
            \label{fig:my_label}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}{Line Following}
Does the previous algorithm work to follow a right curve?
\begin{columns}[t]
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item<1> If on black, keep going straight.
        \item<1> Otherwise, turn left to stay above the line
    \end{enumerate}
    
    \begin{enumerate}
        \item<2> \sout{If on black, keep going straight.}
        \item<2> \sout{Otherwise, turn left to stay above the line}
    \end{enumerate}

    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.7\textheight]{images/color/follow_right_curve.pdf}
            \caption{Follow right curve}
            \label{fig:my_label}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}{Line Following}
We only have two states to work with, that tell us whether or not there's a line beneath us. We also \emph{know} that if we have any plan of following generic lines, we need to be able to make turns in both directions. One way to keep ourselves on track is to follow the edge of the line.
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item If on black, turn left.
        \item If on white, turn right.
    \end{enumerate}
    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/color/follow_edge_line.pdf}
            \caption{Follow edge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}{Line Following Challenge}
\begin{block}{Challenge 4}
    Program the robot to follow the edge of generic line \faClockO~30 min.
\end{block}
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item Follow the left edge of the line indefinitely using the \textbf{Color Mode}\\
        \faFileCodeO~follow\_left\_edge\_color
        \item Follow the right edge of the line indefinitely using the \textbf{Reflected Light Intensity Mode}\\
        \faFileCodeO~follow\_right\_edge\_reflected
        \item Follow the left edge of the line and stop in case you bump into an object using the \textbf{Ambient Light Intensity Mode} \\
        \faFileCodeO~follow\_until\_touch\_ambient
    \end{enumerate}
    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/color/line_following_challenge_diagram.pdf}
            \caption{Follow edge challenge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}[t]{Line Following Challenge}
\begin{exampleblock}{Solution}
    The solution consists of a \textbf{Loop Block} that contains a \textbf{Switch Block} which switches between steering left or right according to the color sensor's output.
\end{exampleblock}

\begin{figure}
    \centering
    \includegraphics<1>[height=0.55\textheight]{images/color/follow_edge_challenge_solution.png}
\end{figure}

\begin{alertblock}<2>{Reflect}
Does your code work with a steeper curves? How can you modify it to do so?
\end{alertblock}
\end{frame}

\end{document}