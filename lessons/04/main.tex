%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------
\documentclass[xcolor={dvipsnames}]{beamer}

\mode<presentation> {
    \usetheme{Madrid}
    \usecolortheme{dolphin}
}
\usepackage[english]{babel}
\usepackage[os=win]{menukeys}
\usepackage[normalem]{ulem}
\usepackage{algorithm2e}
\usepackage{algorithmic}
\usepackage{fontawesome}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{subcaption}
\usepackage{array}
\usepackage{booktabs}
\usepackage{tabularx}
    \newcolumntype{L}{>{\hsize=.5\hsize}X}
    
\AtBeginSection[] {
    \begin{frame}<beamer>
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
    \end{frame}
}

% Center figures inside table cells
\usepackage{cellspace}
\setlength\cellspacetoplimit{4pt}
\setlength\cellspacebottomlimit{4pt}
\newcommand\cincludegraphics[2][]{\raisebox{-0.3\height}{\includegraphics[#1]{#2}}}


%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[Lesson 4]{Programming with Mindstorms EV3} % The short title appears at the bottom of every slide, the full title is only on the title page
\subtitle{Lesson 4}
\author{Rafael Figueiredo Prudencio} % Your name
\institute[UNICAMP] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
    Universidade Estadual de Campinas \\ % Your institution for the title page
    \medskip
    \textit{rafael.prudencio@gmail.com} % Your email address
}
\date{\today} % Date, can be changed to a custom date

\begin{document}
\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\section{Introduction}
\subsection{Recap}

\begin{frame}
\frametitle{Recap}
What do you remember from last class?
\begin{columns}[c]
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item<2-> \textbf{Data Types} What are the different data types? Can you think of an example of each?
            \item<3-> \textbf{Data Wires} Can you still identify what wire corresponds to each type?
            \item<4-> \textbf{Import Blocks} How can you add third-party blocks to your IDE?
            \item<5-> \textbf{RGB} What is the RGB color model? What is the RGB Color Sensor Block useful for?
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{center}
            \includegraphics<3>[width=0.7\textwidth]{images/data/data_wires_example.png}
            \includegraphics<4>[width=0.7\textwidth]{images/color/color_sensor_rgb_block.png}
            \includegraphics<5>[width=0.7\textwidth]{images/color/color_explorer.png}
        \end{center}
    \end{column}
\end{columns}
\end{frame}

\subsection{Lesson Overview}
\begin{frame}{Lesson Overview}
    \begin{enumerate}
        \item Program the robot to follow a line using the \textbf{Color Sensor}
        \item Understand the limitations and different modes of the \textbf{Ultrasonic Sensor}
        \item Program the robot to follow a line using the \textbf{Ultrasonic Sensor}
        \item Use multiple sensors to find gold nuggets in a river
    \end{enumerate}
\end{frame}

\subsection{Line Following}
\begin{frame}{Line Following}
We only have two states to work with, that tell us whether or not there's a line beneath us. We also \emph{know} that if we have any plan of following generic lines, we need to be able to make turns in both directions. One way to keep ourselves on track is to follow the edge of the line.
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item If on black, turn left.
        \item If on white, turn right.
    \end{enumerate}
    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/color/follow_edge_line.pdf}
            \caption{Follow edge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\subsection{Challenge}
\begin{frame}{Line Following Challenge}
\begin{block}{Challenge 1}
    Program the robot to follow the edge of generic line \faClockO~25 min. \\
    \faFileArchiveO~line\_following
\end{block}
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item Follow the left edge of the line indefinitely using the \textbf{Color Mode}\\
        \faFileCodeO~follow\_left\_edge\_color
        \item Follow the right edge of the line indefinitely using the \textbf{Reflected Light Intensity Mode}\\
        \faFileCodeO~follow\_right\_edge\_ref
    \end{enumerate}
    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/color/line_following_challenge_diagram.pdf}
            \caption{Follow edge challenge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}[t]{Line Following Challenge}
\begin{exampleblock}{Solution}
    The solution consists of a \textbf{Loop Block} that contains a \textbf{Switch Block} which switches between steering left or right according to the color sensor's output.
\end{exampleblock}

\begin{figure}
    \centering
    \includegraphics<1>[height=0.55\textheight]{images/color/follow_edge_challenge_solution.png}
\end{figure}

\begin{alertblock}<2>{Reflect}
Does your code work with a steeper curves? How can you modify it to do so?
\end{alertblock}
\end{frame}

\section{Ultrasonic Sensor}
\subsection{What is it?}
\begin{frame}{Ultrasonic Sensor}
The \textbf{Ultrasonic Sensor} measures the distance to an object in front of it. It does this by sending out sound waves and measuring how long it takes the sound to reflect back to the sensor. The sound frequency is too high for you to hear, hence its name 'ultrasonic'.

\begin{block}{Considerations}
\begin{itemize}
    \item The \textbf{Ultrasonic Sensor} works best to detect objects with hard surfaces that reflect sound well. Soft objects, such as cloth, may absorb sound waves. Angled surfaces can also be challenging
    \item The sensor cannot detect objects that are very close (closer than 3cm)
    \item The sensor has a wide field of view
\end{itemize}
\end{block}

\begin{figure}
    \centering
    \includegraphics[height=0.3\textheight]{images/ultrasound/ultrasonic.jpg}
\end{figure}

\end{frame}

\subsection{Modes}
\begin{frame}{Ultrasonic Sensor Modes}
The Ultrassonic Sensor operates on three different modes: \textbf{Distance in Centimeters}, \textbf{Distance in Inches}, and \textbf{Ultrasound Detected}. 

\begin{table}[b]
    \centering
    \begin{tabular}{| m{3.8cm} | m{1.5cm} | m{1.5cm} | m{3cm} |}
         \hline
         \textbf{Data} & \textbf{Type} & \textbf{Range} & \textbf{Notes} \\\hline
         Distance in centimeters & Numeric & 0 to 255 & Distance to object in centimeters \\\hline 
         Distance in  inches & Numeric & 0 to 100 & Distance to object in inches \\\hline
         Ultrasound detected & Logic & True/False & True if another ultrasonic sensor is detected \\\hline
    \end{tabular}
    \caption{Different modes and respective types for Ultrasonic Sensor}
\end{table}
\end{frame}

\subsection{Examples}
\begin{frame}{Ultrasonic Sensor Examples}
How would you program your robot to drive forward until it detects something closer than 20 cm?

\onslide<2>
\begin{figure}
    \centering
    \includegraphics[width=0.95\textwidth]{images/ultrasound/ultrasound_stop_example.png}
    \caption{Move until obstacle is less than 20cm away}
\end{figure}
    
\end{frame}

\begin{frame}{Ultrasonic Sensor Examples}
Can you read the program out loud in plain English?

\begin{figure}
    \centering
    \includegraphics[width=0.95\textwidth]{images/ultrasound/ultrasound_slow_down_example.png}
\end{figure}
    
\end{frame}

\subsection{Challenge}

\begin{frame}{Ultrasonic Sensor Challenge}
\begin{block}{Challenge 2}
    Use the force to help your robot follow a line \faClockO~15 min. Your solution must include comments to showcase your though process. \\
    \faFileArchiveO~ultrasonic\\
    \faFileCodeO~line\_following
\end{block}

\begin{columns}
    \begin{column}{0.5\textwidth}
        \onslide<2>
        \begin{alertblock}{Tips}
        How can we switch between three conditions?
        \end{alertblock}
    \end{column}
    \begin{column}{0.5\textwidth}
        \onslide<1->
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/ultrasound/ultrasonic_line_following.pdf}
            \caption{Follow line challenge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}[t]{Line Following Challenge}
\begin{exampleblock}{Solution}
    The solution consists of a \textbf{Loop Block} with two nested \textbf{Switch Blocks} that alternate between three states: move forward, steer right, and steer left. The steering conditions are arbitrary.
\end{exampleblock}

\begin{figure}
    \centering
    \includegraphics[height=0.55\textheight]{images/ultrasound/ultrasound_line_following_solution.png}
\end{figure}

\end{frame}

\section{Putting it together}
\subsection{Gold Digging}
\begin{frame}{Gold Digging Challenge}
\begin{block}{Challenge 2}
    Program you robot to look for three gold nuggets in the river (table) without falling from it. Every time the robot finds a nugget it should celebrate. After finding all three, the robot should stop looking. The nuggets aren't fixed in place, their locations are randomly shuffled every try.  \faClockO~40 min \\
    \faFileArchiveO~putting\_it\_together\\
    \faFileCodeO~gold\_digging
\end{block}
    
\begin{figure}
    \centering
    \includegraphics[height=0.33\textheight]{images/challenges/gold_digging.pdf}
    \caption{Gold digging challenge}
\end{figure}

\end{frame}

\begin{frame}[t]{Gold Digging Solution}
This challenge is composed of multiple smaller challenges. 

\begin{itemize}
    \item How do I know if I found a nugget?
    \item How do I know if I'm about to fall?
    \item How can I explore the entire river?
    \item How do I keep exploring after finding a cliff?
\end{itemize}

\begin{exampleblock}{Solution}
    One way to solve this challenge is to use parallel execution. We need to keep exploring the river until we find three nuggets, so we need one sequence of blocks to always keep exploring the river and another to be on the lookout for a gold nugget. In order to do so we use two \textbf{Start Blocks} and a \textbf{Loop Interrupt Block} to signal the exploration routine whenever it found a nugget. We use the \textbf{Ultrasonic Sensor} to detect cliffs and the \textbf{Color Sensor} to identify the nuggets.
\end{exampleblock}

\end{frame}

\begin{frame}{Gold Digging Solution}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/challenges/gold_digging_solution.png}
    \caption{Solution to gold digging challenge}
\end{figure}

\end{frame}

\end{document}