%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------
\documentclass[xcolor={dvipsnames}]{beamer}

\mode<presentation> {
    \usetheme{Madrid}
    \usecolortheme{dolphin}
}
\usepackage[english]{babel}
\usepackage[os=win]{menukeys}
\usepackage[normalem]{ulem}
\usepackage{algorithm2e}
\usepackage{algorithmic}
\usepackage{fontawesome}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{subcaption}
\usepackage{array}
\usepackage{booktabs}
\usepackage{tabularx}
    \newcolumntype{L}{>{\hsize=.5\hsize}X}
    
\AtBeginSection[] {
    \begin{frame}<beamer>
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
    \end{frame}
}

% Center figures inside table cells
\usepackage{cellspace}
\setlength\cellspacetoplimit{4pt}
\setlength\cellspacebottomlimit{4pt}
\newcommand\cincludegraphics[2][]{\raisebox{-0.3\height}{\includegraphics[#1]{#2}}}


%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[Lesson 5]{Programming with Mindstorms EV3} % The short title appears at the bottom of every slide, the full title is only on the title page
\subtitle{Lesson 5}
\author{Rafael Figueiredo Prudencio} % Your name
\institute[UNICAMP] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
    Universidade Estadual de Campinas \\ % Your institution for the title page
    \medskip
    \textit{rafael.prudencio@gmail.com} % Your email address
}
\date{\today} % Date, can be changed to a custom date

\begin{document}
\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\section{Introduction}
\subsection{Recap}

\begin{frame}
\frametitle{Recap}
What do you remember from last class?
\begin{columns}[c]
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item<2-> \textbf{Line Following} How can we follow a line using the \textbf{Color Sensor}?
            \item<3-> \textbf{Ultrasonic Sensor} What are the different modes it operates on? What are some limitations?
            \item<4-> \textbf{Using the Force} How can we use the \textbf{Ultrasonic Sensor} to guide the robot's movement? 
            \item<5-> \textbf{Challenge} What challenge did we talk about at the end of the class?
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{center}
            \includegraphics<2>[height=1.1\textwidth]{images/color/follow_edge_line.pdf}
            \includegraphics<3>[width=0.7\textwidth]{images/ultrasound/ultrasonic.jpg}
            \includegraphics<4>[width=0.7\textwidth]{images/ultrasound/ultrasonic_line_following.pdf}
            \includegraphics<5>[width=0.7\textwidth]{images/challenges/gold_digging.pdf}
        \end{center}
    \end{column}
\end{columns}
\end{frame}

\subsection{Lesson Overview}
\begin{frame}{Lesson Overview}
    \begin{enumerate}
        \item Program the robot to follow a line using the \textbf{Color Sensor}
        \item Understand the limitations and different modes of the \textbf{Ultrasonic Sensor}
        \item Program the robot to follow a line using the \textbf{Ultrasonic Sensor}
        \item Use multiple sensors to find gold nuggets in a river
    \end{enumerate}
\end{frame}


\section{Putting it together}

\subsection{Maze}
\begin{frame}{Maze Challenge}
\begin{block}{Challenge 1}
    Program your robot to escape the maze. The robot should follow the path traced with red arrows. Note that when the arrows don't touch the wall, the robot shouldn't do so either. In order to navigate through the passage, the robot should use the black line to guide its path.
    \faClockO~40 min \\
    \faFileArchiveO~putting\_it\_together\\
    \faFileCodeO~maze
\end{block}
    
\begin{figure}
    \centering
    \includegraphics[height=0.4\textheight]{images/challenges/maze_challenge.pdf}
    \caption{Maze challenge}
\end{figure}
\end{frame}

\begin{frame}[t]{Maze Challenge Solution}

\begin{exampleblock}{Solution}
    This challenge is meant to exercise your ability to put together what we've learned until now. One way to solve it is to move forward until the \textbf{Touch Sensor} is pressed and retreat slightly. Then switch to the line following routine using the \textbf{Color Sensor} for a specified amount of time after which you can use the \textbf{Ultrasonic Sensor} to detect the soft wall. Finally, you should turn appropriately to reach the exit.
\end{exampleblock}
\begin{figure}
    \centering
    \includegraphics[height=0.35\textheight]{images/challenges/maze_challenge.pdf}
    \caption{Maze challenge}
\end{figure}
\end{frame}

\begin{frame}{Maze Challenge Solution}
    \begin{figure}
        \centering
        \includegraphics[width=0.95\textwidth]{images/challenges/maze_challenge_solution.png}
        \caption{Maze challenge solution}
    \end{figure}
\end{frame}

\subsection{Gold Digging}
\begin{frame}{Gold Digging Challenge}
\begin{block}{Challenge 2}
    Program you robot to look for three gold nuggets in the river (table) without falling from it. Every time the robot finds a nugget it should celebrate. After finding all three, the robot should stop looking. The nuggets aren't fixed in place, their locations are randomly shuffled every try.  \faClockO~40 min \\
    \faFileArchiveO~putting\_it\_together\\
    \faFileCodeO~gold\_digging
\end{block}
    
\begin{figure}
    \centering
    \includegraphics[height=0.33\textheight]{images/challenges/gold_digging.pdf}
    \caption{Gold digging challenge}
\end{figure}

\end{frame}

\begin{frame}[t]{Gold Digging Solution}
This challenge is composed of multiple smaller challenges. 

\begin{itemize}
    \item How do I know if I found a nugget?
    \item How do I know if I'm about to fall?
    \item How can I explore the entire river?
    \item How do I keep exploring after finding a cliff?
\end{itemize}

\begin{exampleblock}{Solution}
    One way to solve this challenge is to use parallel execution. We need to keep exploring the river until we find three nuggets, so we need one sequence of blocks to always keep exploring the river and another to be on the lookout for a gold nugget. In order to do so we use two \textbf{Start Blocks} and a \textbf{Loop Interrupt Block} to signal the exploration routine whenever it found a nugget. We use the \textbf{Ultrasonic Sensor} to detect cliffs and the \textbf{Color Sensor} to identify the nuggets.
\end{exampleblock}

\end{frame}

\begin{frame}{Gold Digging Solution}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/challenges/gold_digging_solution.png}
    \caption{Solution to gold digging challenge}
\end{figure}

\end{frame}

\end{document}