%----------------------------------------------------------------------------------------
%	PACKAGES AND THEMES
%----------------------------------------------------------------------------------------
\documentclass[xcolor={dvipsnames}]{beamer}

\mode<presentation> {
    \usetheme{Madrid}
    \usecolortheme{dolphin}
}
\usepackage[english]{babel}
\usepackage[os=win]{menukeys}
\usepackage[normalem]{ulem}
\usepackage{algorithm2e}
\usepackage{algorithmic}
\usepackage{fontawesome}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{subcaption}
\usepackage{tabularx}
    \newcolumntype{L}{>{\hsize=.5\hsize}X}
    
\AtBeginSection[] {
    \begin{frame}<beamer>
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
    \end{frame}
}

% Center figures inside table cells
\usepackage{cellspace}
\setlength\cellspacetoplimit{4pt}
\setlength\cellspacebottomlimit{4pt}
\newcommand\cincludegraphics[2][]{\raisebox{-0.3\height}{\includegraphics[#1]{#2}}}


%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[Lesson 3]{Programming with Mindstorms EV3} % The short title appears at the bottom of every slide, the full title is only on the title page
\subtitle{Lesson 3}
\author{Rafael Figueiredo Prudencio} % Your name
\institute[UNICAMP] % Your institution as it will appear on the bottom of every slide, may be shorthand to save space
{
    Universidade Estadual de Campinas \\ % Your institution for the title page
    \medskip
    \textit{rafael.prudencio@gmail.com} % Your email address
}
\date{\today} % Date, can be changed to a custom date

\begin{document}
\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------

\section{Introduction}
\subsection{Recap}

\begin{frame}
\frametitle{Recap}
What do you remember from last class?
\begin{columns}[c]
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item<2-> \textbf{Flow Blocks} What are the different types of flow blocks? When should you use each one?
            \item<3-> \textbf{Color Sensor} What are the different modes for the color sensor? When should you use each mode?
            \item<4-> \textbf{Action Blocks} How can you display an image or play a sound with your EV3s?
            \item<5-> \textbf{Challenge} Where did we stop last class? What difficulties were we facing?
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{center}
            \includegraphics<2>[width=0.7\textwidth]{images/flow/loop_block.png}
            \includegraphics<3>[width=0.7\textwidth]{images/color/color_block.png}
            \includegraphics<4>[width=0.7\textwidth]{images/action/action_blocks.png}
            \includegraphics<5>[width=0.7\textwidth]{images/color/color_racing_challenge_diagram.pdf}
        \end{center}
    \end{column}
\end{columns}
\end{frame}

\subsection{Lesson Overview}
\begin{frame}{Lesson Overview}
    \begin{enumerate}
        \item Understand what are and the differences between \textbf{Data Types}
        \item Practice using \textbf{Data Wires} to transmit data between blocks
        \item Learn how to \textbf{Import Blocks}
        \item Understand the \textbf{RGB} color model and how to use the \textbf{RGB Color Block}
        \item Program a robot to race on different racetracks
        \item Program a robot to follow a line
    \end{enumerate}
\end{frame}

\section{Handling Data}
\subsection{Data Types}
\begin{frame}{Data Types}
Data values in a program can have one of five different types: \textbf{Numeric}, \textbf{Logic}, \textbf{Text}, \textbf{Numeric Array}, \textbf{Logic Array}. The \textbf{Inputs} and \textbf{Outputs} of Programming Blocks and Data Wires all have one of these types.

\begin{itemize}
    \item \textbf{Numeric} type represents a number. Numeric values can be positive or negative, and have digits after the decimal point.
    \item<2-> \textbf{Logic} type represents a True or False value. 
    \item<3-> \textbf{Text} type represents a text string, which is a sequence of text characters. A Text can be a word, single letter, or a sentence.
    \item<4-> \textbf{Array} types represent a list of elements of the same type. Could be a list of numeric or logic elements.
\end{itemize}

\begin{table}[t]
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Type} &  \textbf{Examples} \\\hline
        Numeric & $3$, $1.25$, $-75$, $12345.678$, $-0.001$ \\\hline
        \onslide<2->{Logic & True, False} \\\hline
        \onslide<3->{Text & Hello, A, Longer text} \\\hline
        \onslide<4->{Array & [], $[3]$, $[2;3;5]$, [True, False]} \\\hline
    \end{tabular}
    \caption{Data types for the EV3}
\end{table}
\end{frame}



\subsection{Data Wires}
\begin{frame}
\frametitle{Data Wires}
A \textbf{Data Wire} allows you to supply an input value for a programming block using an output value from another block in your program. This allows you to create interactions between blocks and program more complex behaviors.

This is where \textcolor{Goldenrod}{sensor blocks} come in handy. They allow you to handle the data collected from a sensor directly. What does the program below do?

\begin{figure}
    \centering
    \includegraphics[height=0.43\textheight]{images/data/data_wires_example.png}
    \caption{Data Wires use case example}
\end{figure}
\end{frame}

\begin{frame}{Data Wires}
Data Wires carry values from one block to another. Each Data Wire has a type, which is determined by the output type of the block at the start of the wire. \textbf{Data Wires}, block \textbf{inputs}, and block \textbf{outputs} have a different appearance depending on their type.
    
\begin{table}[]
    \centering
    \begin{tabular}{|c|Sc|Sc|Sc|}
        \hline
        \textbf{Type} & \textbf{Block Input} & \textbf{Block Output} & \textbf{Data Wire} \\\hline
        Logic & \cincludegraphics[scale=0.5]{images/data/logic_port.png} & \cincludegraphics[scale=0.5, angle=180, origin=c]{images/data/logic_port.png} & \cincludegraphics{images/data/logic_wire.png} \\\hline

        Numeric & \cincludegraphics[scale=0.5]{images/data/numeric_port.png} & \cincludegraphics[scale=0.5, angle=180, origin=c]{images/data/numeric_port.png} & \cincludegraphics{images/data/numeric_wire.png} \\\hline
        
        Text & \cincludegraphics[scale=0.5]{images/data/text_port.png} & \cincludegraphics[scale=0.5, angle=180, origin=c]{images/data/text_port.png} & \cincludegraphics{images/data/text_wire.png} \\\hline
        
        Logic Array & \cincludegraphics[scale=0.5]{images/data/logic_array_port.png} & \cincludegraphics[scale=0.5, angle=180, origin=c]{images/data/logic_array_port.png} & \cincludegraphics{images/data/logic_array_wire.png} \\\hline
        
        Numeric Array & \cincludegraphics[scale=0.5]{images/data/numeric_array_port.png} & \cincludegraphics[scale=0.5, angle=180, origin=c]{images/data/numeric_array_port.png} & \cincludegraphics{images/data/numeric_array_wire.png} \\\hline
    \end{tabular}
    \caption{Block inputs, outputs and data wires' appearance in the IDE}
\end{table}
\end{frame}

\begin{frame}{Data Wires}
When programming with Data Wires, me must pay attention not to make silly mistakes.
\begin{itemize}
    \item The block with the output must come before the one with the input in the program
    \item You cannot connect more than one data wire to the same input
    \item Hover the wire in the IDE when your program is running in order to see its value
\end{itemize}    

\begin{figure}
    \centering
    \includegraphics[height=0.42\textheight]{images/data/hover_wire_value.png}
    \caption{Data wire value during program execution}
\end{figure}
\end{frame}

\section{Block Import}
\begin{frame}{Block Import}
There are manufacturers besides LEGO who create sensors and add-ons that can be used with the EV3 Brick. Often they create \textbf{Programming Blocks} that can be with their products or that add functionality to existing sensors. 

\begin{enumerate}
    \item Go to \menu{Tools > Block Import} 
    \item Press \menu{Browse} and look for the file \path{ColorSensorRGB-v1.00.ev3b} in \path{C:\Users\...\Documents\ev3-lessons\assets}
    \item Press \menu{Import} followed by \menu{Ok} and then restart the IDE
    \item In the yellow tab you should now see a new block called \menu{Color Sensor RGB}
\end{enumerate}

\begin{figure}
    \centering
    \includegraphics[scale=0.8]{images/color/color_sensor_rgb_block.png}
    \caption{Color Sensor RGB Block}
\end{figure}
\end{frame}

\section{RGB Block}
\subsection{Understanding RGB}
\begin{frame}{RGB}
    In order to understand how to use the \textbf{RGB Color Sensor Block} we must first look into how colors are composed in an additive color model. Navigate to \url{http://web.stanford.edu/class/cs101/image-rgb-explorer.html} and play around with the \textbf{Red}, \textbf{Green}, and \textbf{Blue} color sliders. 
    
    \begin{figure}
        \centering
        \includegraphics[width=0.6\textwidth]{images/color/color_explorer.png}
        \caption{RGB color explorer}
    \end{figure}
\end{frame}

\subsection{Calibration}
\begin{frame}{Color Calibration}
    When using the RGB block, we don't have predefined thresholds for each color, so we have to define our own. In order to do so, we need to measure the output values for the block when hovering a colored object with the \textbf{Color Sensor}. One way to do that is to display the \textbf{RGB Blocks}'s output in the EV3 Brick display. \\
    \faFileArchiveO~color.ev3 \\
    \faFileCodeO~rgb\_calibration.ev3p \\

    
    \begin{figure}
        \centering
        \includegraphics[width=0.95\textwidth]{images/color/color_sensor_rgb_calibration.png}
        \caption{Program to calibrate RGB color block using the brick's display}
    \end{figure}
\end{frame}

\subsection{Detecting Colors}
\begin{frame}{Detecting Color}
    How would you use the \textbf{RGB Color Block} to program your robot to detect a specific color? By detect I mean output \textbf{True} when it sees it and \textbf{False} when it doesn't.
    
    \onslide<2->
    Let's say we use our calibration technique and measure the following colors for each channel of the \textbf{RGB Color Block} when above a green line

    \begin{itemize}
        \item \textcolor{red}{\textbf{Red}}: $[17, 18]$
        \item \textcolor{green}{\textbf{Green}}: $[68, 69]$
        \item \textcolor{blue}{\textbf{Blue}}: $[65, 66]$
    \end{itemize}   
\end{frame}

\begin{frame}{Detecting Color}
    How would you express mathematically a color belonging to the these three thresholds?
    
    Let's say $\textcolor{red}{r}$, $\textcolor{green}{g}$, and $\textcolor{blue}{b}$ are symbols representing the output of the RGB color sensor for red, green, and blue, respectively.

\begin{algorithm}[H]
    \begin{algorithmic}[1]
    \IF{$\textcolor{red}{r} \ge 17$ \& $\textcolor{red}{r} \le 18$ \& $\textcolor{green}{g} \ge 68$ \& $\textcolor{green}{g} \le 69$ \& $\textcolor{blue}{b} \ge 65$ \& $\textcolor{blue}{b} \le 66$}
        \STATE True
    \ELSE 
        \STATE False
    \ENDIF
    \end{algorithmic}
\caption{Pseudocode to detect green}
\end{algorithm}
~\\
Are you guys familiar with these mathematical operators? What do $\ge$ and $\le$ mean? How about logical operators? What does it mean to $\&$ or \texttt{and} two logical values?
\end{frame}

\begin{frame}{Data Operations}
    For brevity, we won't be looking at how individual \textbf{Data Operation Blocks} work, but I expect you to use them to handle data throughout your programs. They allow you to directly use sensor data as inputs to compute complex functions.
    
    \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{images/color/rgb_color_detection_sc.png}
        \caption{Detecting a color using a single channel from the RGB Color Sensor Block}
    \end{figure}
\end{frame}

\begin{frame}{Two-Channel Color Detection}
    \begin{block}{Challenge 1}
        Program your robot to detect the green line using two of the outputs of the RGB color sensor. \faClockO~20 min. \\
        \faFileCodeO~ rgb\_color\_detection\_dc.ev3p
    \end{block}
\end{frame}

\begin{frame}{Two-Channel Color Detection}
    \begin{exampleblock}{Solution}
    In order to use two channels, we must add two more \textbf{Compare Blocks} and two more \textbf{Logical Blocks}.
    \end{exampleblock}
    
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{images/color/rgb_color_detection_dc.png}
        \caption{RGB color detection with two channels}
    \end{figure}
\end{frame}


\section{Color Racing Challenge}
\begin{frame}{Color Racing Challenge}
    \begin{block}{Challenge 2}
        Let's now revisit last class' challenge. We want to build a robot capable of functioning on different racetracks. The finish lines can be \textbf{green}, \textbf{blue} or \textbf{red}. After crossing the finish line, the robot should play what color he saw when he crossed it. \faClockO~30 min. \\
        \faFileCodeO~racing\_challenge
    \end{block}
    
    \begin{figure}
        \centering
        \includegraphics[height=0.45\textheight]{images/color/color_racing_challenge_diagram.pdf}
        \caption{Color racing challenge diagram}
    \end{figure}
\end{frame}

\begin{frame}{Color Racing Challenge}
    \begin{exampleblock}{Solution}
        In order to move the robot until we detect a colored line, we have to use the \textbf{On} and \textbf{Off} modes of the \textbf{Motor Block}. Ideally, we would have a \textbf{Wait on Numeric} mode that allows us to keep moving forward until a certain numeric value is reached. Since we don't, we must use an infinite loop for the \textbf{Motor On} and use a \textbf{Loop Interrupt} when a certain color is detected. The color detection can be done similarly to the examples shown previously, but we have to find a way to combine different colors into a single numeric value. This can be done through multiplication, since \textbf{True} is equivalent to the numeric $1$. 
    \end{exampleblock}
    
    \begin{figure}
        \centering
        \includegraphics[height=0.25\textheight]{images/color/racing_challenge_solution_motor.png}
        \caption{Solution to move robot in racing challenge}
    \end{figure}
\end{frame}

\begin{frame}{Color Racing Challenge}
    \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{images/color/racing_challenge_solution_color.png}
        \caption{Solution to read colors and play sound using the RGB Color block}
    \end{figure}
\end{frame}

\section{Line Following}
\subsection{Approach}
\begin{frame}{Line Following}
How would you program a robot to follow a straight line until its end?
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{itemize}
        \item What kind of questions can we ask?
        \item<2-> Are we on the line? If so, continue. Otherwise, stop.
    \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.7\textheight]{images/color/follow_straight_line.pdf}
            \caption{Follow straight line}
        \end{figure}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}{Line Following}
How can we change our algorithm to follow a curved line?
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item If on black, keep going straight.
        \item Otherwise, turn left to stay above the line
    \end{enumerate}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.7\textheight]{images/color/follow_left_curve.pdf}
            \caption{Follow left curve}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}{Line Following}
Does the previous algorithm work to follow a right curve?
\begin{columns}[t]
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item<1> If on black, keep going straight.
        \item<1> Otherwise, turn left to stay above the line
    \end{enumerate}
    
    \begin{enumerate}
        \item<2> \sout{If on black, keep going straight.}
        \item<2> \sout{Otherwise, turn left to stay above the line}
    \end{enumerate}

    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.7\textheight]{images/color/follow_right_curve.pdf}
            \caption{Follow right curve}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}{Line Following}
We only have two states to work with, that tell us whether or not there's a line beneath us. We also \emph{know} that if we have any plan of following generic lines, we need to be able to make turns in both directions. One way to keep ourselves on track is to follow the edge of the line.
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item If on black, turn left.
        \item If on white, turn right.
    \end{enumerate}
    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/color/follow_edge_line.pdf}
            \caption{Follow edge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\subsection{Challenge}
\begin{frame}{Line Following Challenge}
\begin{block}{Challenge 4}
    Program the robot to follow the edge of generic line \faClockO~15 min.
\end{block}
\begin{columns}
    \begin{column}{0.5\textwidth}
    \begin{enumerate}
        \item Follow the left edge of the line indefinitely using the \textbf{Color Mode}\\
        \faFileCodeO~follow\_left\_edge\_color
        \item Follow the right edge of the line indefinitely using the \textbf{Reflected Light Intensity Mode}\\
        \faFileCodeO~follow\_right\_edge\_reflected
    \end{enumerate}
    
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \includegraphics[height=0.55\textheight]{images/color/line_following_challenge_diagram.pdf}
            \caption{Follow edge challenge}
        \end{figure}
    \end{column}
\end{columns}
    
\end{frame}

\begin{frame}[t]{Line Following Challenge}
\begin{exampleblock}{Solution}
    The solution consists of a \textbf{Loop Block} that contains a \textbf{Switch Block} which switches between steering left or right according to the color sensor's output.
\end{exampleblock}

\begin{figure}
    \centering
    \includegraphics<1>[height=0.55\textheight]{images/color/follow_edge_challenge_solution.png}
\end{figure}

\begin{alertblock}<2>{Reflect}
Does your code work with a steeper curves? How can you modify it to do so?
\end{alertblock}
\end{frame}

\end{document}